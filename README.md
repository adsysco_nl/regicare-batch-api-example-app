# RegiCare OAuth demo application

## Getting started
1. Clone this repository
2. Run `composer install`
3. Create an oAuth client at the oAuth provider
4. Copy `.env.example` to `.env`
5. Fill out the required `.env` variables

## Trying it out
Visit `http://path-to-this-app.com/get-token`.
This get a fresh token.

 After a succesfull attempt, you can go to
 `/persons` to display some basic person information that is gathered from the API.
