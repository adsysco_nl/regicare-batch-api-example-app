<?php

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/get-token', function (Request $request) {
    $response = Http::asForm()->post(env('OAUTH_URL') . '/token', [
        'grant_type' => 'client_credentials',
        'client_id' => env('OAUTH_CLIENT_ID'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
        'scope' => '',
    ]);

    $request->session()->put('accessToken', $response->json()['access_token']);
    redirect('persons');
});


Route::get('persons', function (Request $request) {
    $accessToken = $request->session()->get('accessToken');

    $response = Http::withHeaders([
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$accessToken,
    ])->get(env('API_URL') . '/batch/persons');

    return view('persons', ['persons' => $response->json()['data']]);
});
